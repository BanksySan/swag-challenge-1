﻿using NUnit.Framework;

namespace SwagChallenge_1.Tests
{
    [TestFixture]
    class SolutionTests
    {
        [Test]
        public void Calculate_Tests()
        {
            var populations = new string[] {
                "5,6,11,13,19,20,25,26,28,37",
                "37,81,86,91,97,108,109,112,112,114,115,117,121,123,141",
                "266,344,375,399,409,433,436,440,449,476,502,504,530,584,587",
                "809,816,833,849,851,961,976,1009,1069,1125,1161,1172,1178,1187,1208,1215,1229,1241,1260,1373"
            };

            var solution = new Solution(populations);
            var results = solution.Calculate();

            Assert.AreEqual(10, results[0]);
            Assert.AreEqual(23, results[1]);
            Assert.AreEqual(84, results[2]);
            Assert.AreEqual(170, results[3]);
        }
    }
}

﻿
namespace SwagChallenge_1
{
    public interface ISolution
    {
        int[] Calculate();
    }
}

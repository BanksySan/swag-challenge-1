﻿namespace SwagChallenge_1
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Solution : ISolution
    {
        private readonly int[] _answers;

        public Solution(string[] populations)
        {
            var _populations = populations.Select(x => x.Split(',', ' ').Select(int.Parse).ToArray()).ToArray();
            var answers = new List<int>();

            foreach (var population in _populations) { answers.Add(CalculateStdDev(population)); }

            _answers = answers.ToArray();

            Calculate();
        }

        public int[] Calculate( )
        {
            return _answers;
        }

        private int CalculateStdDev(int[] population)
        {
            var average = population.Sum() / population.Length;
            var deviations = new List<double>();
            foreach (var value in population) { deviations.Add(Math.Pow(value - average, 2)); }

            var variance = deviations.Sum() / population.Length;

            var stdDev = Math.Sqrt(variance);

            return (int) Math.Round(stdDev);
        }
    }
}